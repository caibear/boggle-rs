#![feature(box_syntax)]
#![feature(core_intrinsics)]
#![feature(atomic_mut_ptr)]
#![feature(pointer_byte_offsets)]
#![feature(once_cell)]
#![feature(test)]

extern crate test;

mod board;
mod trie;

use crate::board::*;
use crate::trie::*;
use bitintr::Blsr;
use crunchy::*;
use std::mem;
use std::sync::atomic::*;
use std::sync::{Barrier, Mutex, OnceLock};
use std::time::{Duration, Instant};

/// Maximum number of found words printed (longest ones).
const MAX_OUTPUT: usize = 100;

/// Use compressed version of trie ~3.2mb vs ~26mb.
/// Not optimal for larger boards because it can't delete found words.
const COMPRESSED_TRIE: bool = true;

/// Import compressed trie from file.
const IMPORT: bool = false;

/// Use SIMD version of CTrie search (slightly slower).
const SIMD: bool = false;

/// Use iterative version of CTrie search (much slower).
const ITERATIVE: bool = false;

/// Uses an invalid bit instead of replacing invalids with 0 to save instructions.
/// Only works with [`COMPRESSED_TRIE`].
pub const USE_INVALID_BIT: bool = COMPRESSED_TRIE;

fn main() {
    let board = Board::new();
    println!("Created a {0} by {0} board!\n", SIZE);
    println!("{}", board);

    let mut word_count = 168845;
    let (duration, words) = if COMPRESSED_TRIE {
        let c_trie = if IMPORT {
            let imported = import_c_trie();
            assert_eq!(imported.checksum(), 1362817002);
            imported
        } else {
            create_c_trie()
        };

        println!("Loaded compressed trie!");
        (search(board, &c_trie), c_trie.collect())
    } else {
        let (trie, wc, node_count) = create_trie();
        word_count = wc;
        println!("Loaded {} words with {} nodes!", word_count, node_count);
        (search(board, &trie), trie.collect(&create_trie().0))
    };

    print_found(words, word_count, duration);
}

fn create_c_trie() -> CTrie {
    CTrie::from_trie(&create_trie().0)
}

// TODO look into using build script to export automatically.
#[test]
fn export_compressed() {
    use std::fs::File;
    use std::io::Write;

    let c_trie = create_c_trie();
    let exported = c_trie.export();
    File::create("src/c_trie.txt")
        .unwrap()
        .write_all(exported)
        .unwrap()
}

fn import_c_trie() -> CTrie {
    #[cfg(test)]
    {
        let _ = CTrie::import; // Suppress unused warning.
        unimplemented!();
    }
    #[cfg(not(test))]
    CTrie::import(include_bytes!("c_trie.txt"))
}

/// Create a trie from word_list.txt.
fn create_trie() -> (Trie, usize, usize) {
    let mut trie = Trie::new();
    let mut word_count = 0;
    let mut node_count = 1;

    for s in include_str!("word_list.txt").split_whitespace() {
        word_count += 1;
        node_count += trie.add(s.as_bytes());
    }

    (trie, word_count, node_count)
}

/// A barrier that can have lower latency than [`std::sync::Barrier`].
struct SpinBarrier {
    count: usize,
    exact: bool,
    wait: AtomicUsize,
    barrier: OnceLock<Option<Barrier>>,
}

impl SpinBarrier {
    /// [`count`] is how many threads must pass for spin to return.
    /// If [`exact`] is true, spin must not be called more than count times.
    fn new(count: usize, exact: bool) -> Self {
        assert!(count < usize::MAX / 4);
        Self {
            count,
            exact,
            wait: count.into(),
            barrier: OnceLock::new(),
        }
    }

    /// Spin will wait until [`count`] threads have arrived by performing up to [`spin`] iterations
    /// and then defaulting to [`std::sync::Barrier`].
    fn spin(&self, spin: usize) {
        if spin > 0 {
            let w = self.wait.fetch_sub(1, Ordering::AcqRel);
            if self.exact {
                assert_ne!(w, 0);
            }
        }

        for _ in 0..spin {
            let w = self.wait.load(Ordering::Acquire);
            if w == 0 || (!self.exact && w > usize::MAX / 4 * 3) {
                return;
            } else if w >= usize::MAX / 4 {
                break;
            }
            std::hint::spin_loop();
        }

        let barrier = self.barrier.get_or_init(|| {
            // Detect possible finish while locking backup and return no barrier.
            let w = self.wait.fetch_add(usize::MAX / 2, Ordering::AcqRel);
            // println!("barrier");
            (w != 0 || (!self.exact || w > usize::MAX / 4 * 3)).then(|| Barrier::new(self.count))
        });
        if let Some(barrier) = barrier {
            barrier.wait();
        }
    }
}

/// Searches the board and marks which words were found in the trie.
/// It returns how long the word searching took.
/// The search duration only includes calculations that require both the trie and the board.
fn search<T: TrieTrait>(ref board: Box<Board>, trie: &T) -> Duration {
    let ref counter = AtomicUsize::new(0);
    let threads = num_cpus::get();

    let ref ready_wait = Barrier::new(threads);
    let ref warmup_wait = SpinBarrier::new(threads / 2, false);
    let ref end_wait = AtomicUsize::default();
    let ref min_start = Mutex::new(Option::<Instant>::None);
    let (ref end_input, ref end_output) = std::sync::mpsc::sync_channel(0);

    // We use core ids and a warmup barrier of threads / 2 to reduce latency on hyper-threaded cpus.
    let core_ids = core_affinity::get_core_ids().unwrap();

    crossbeam::scope(|s| {
        for (_, core_id) in (0..threads).zip(core_ids.iter().copied().cycle()) {
            s.spawn(move |_| {
                core_affinity::set_for_current(core_id);

                // Force heap based copy.
                let mut board = box Board { board: board.board };

                ready_wait.wait();
                warmup_wait.spin(10000);
                let start = Instant::now();

                'outer: loop {
                    // Claim multiple indices at once (counter can be quite contended).
                    const STRIDE: usize = const_utils::max(SIZE / 5, 1);
                    let c = counter.fetch_add(STRIDE, Ordering::Relaxed);
                    if c >= SIZE * SIZE {
                        break;
                    }

                    // Non padded coordinates.
                    let cy = c % SIZE;
                    let cx = c / SIZE;

                    // Padded coordinates.
                    let mut y = cy + 1;
                    let mut index = y + (cx + 1) * PADDED;

                    for _ in 0..STRIDE {
                        debug_assert_eq!(index % PADDED, y);
                        debug_assert_eq!((index - y) % PADDED, 0);

                        unsafe {
                            // Pointer into board instead of index (less variables).
                            let ptr = board.board.as_mut_ptr().add(index);

                            // Search in trie impl.
                            trie.search_ptr(ptr);
                        }

                        index += 1;
                        if y == PADDED - 2 {
                            y = 1;
                            if index >= PADDED * (PADDED - 1) {
                                break 'outer;
                            }
                        } else {
                            y += 1;
                        }
                    }
                }

                let e = end_wait.fetch_add(1, Ordering::AcqRel) + 1;
                let end = (e == threads).then(Instant::now);

                let mut min_start = min_start.lock().unwrap();
                if let Some(min_start) = min_start.as_mut() {
                    *min_start = (*min_start).min(start);
                } else {
                    *min_start = Some(start);
                }
                if let Some(end) = end {
                    end_input.send(end).unwrap()
                }
                // TODO see if thread destruction impacts the measured duration.
            });
        }

        let end = end_output.recv().unwrap();
        let start: Instant = min_start.lock().unwrap().unwrap();
        end - start
    })
    .unwrap()
}

trait TrieTrait: Send + Sync {
    /// Searches a given ptr on the board.
    /// Returns if the search is detected as complete.
    unsafe fn search_ptr(&self, ptr: *mut u8) -> bool;
}

impl TrieTrait for Trie {
    unsafe fn search_ptr(&self, ptr: *mut u8) -> bool {
        let idx = *ptr;
        debug_assert_ne!(idx, INVALID);
        assert!(!USE_INVALID_BIT, "regular trie cannot use invalid bit");

        if let Some(n) = self.index_child(idx) {
            let n = self.offset(n.get());
            if find(n, ptr) && self.delete_child(idx) {
                return true;
            }
        }

        false
    }
}

/// Recursively finds words connected by adjacent letters.
/// Doesn't use the same letter twice.
/// ptr is a pointer into the 2D board which is nul terminated with padding along all edges.
unsafe fn find(node: &Node, ptr: *mut u8) -> bool {
    if node.mark() {
        return true;
    }

    // Instead of separate searched array just take the letters out.
    let old = mem::replace(&mut *ptr, INVALID);

    unroll! {
        for a in 0..3 {
            for b in 0..3 {
                let i = a as isize - 1;
                let j = b as isize - 1;

                // Compiled out because loop is unrolled.
                if i == 0 && j == 0 {
                    continue;
                }

                // Doesn't need any bounds checks because board is padded with invalids.
                let new_ptr = (ptr as isize + i * PADDED as isize + j) as *mut u8;
                let idx = *new_ptr;

                // If idx is invalid the child will be None (saves 1 branch).
                if let Some(sub) = node.index_child(idx) {
                    // Uses pointer arithmetic.
                    let sub = node.offset(sub.get());
                    if find(sub, new_ptr) && node.delete_child(idx) {
                        *ptr = old;
                        return true;
                    }
                }
            }
        }
    }

    *ptr = old;
    false
}

impl TrieTrait for CTrie {
    unsafe fn search_ptr(&self, ptr: *mut u8) -> bool {
        if ITERATIVE {
            c_find_iterative(self.root(), ptr);
            return false;
        }

        let idx = *ptr;
        debug_assert_ne!(idx, INVALID);

        let root = self.root();
        let children_ptr = root.children();
        let data = root.load_data_u64(); // TODO compiler can't pull this out of the loop.

        if let Some(n) = CNode::get_child2(children_ptr, idx, data) {
            if SIMD {
                c_find_simd(n, ptr)
            } else {
                c_find(n, ptr)
            }
        }

        false
    }
}

unsafe fn c_find_iterative(node: &CNode, mut ptr: *mut u8) {
    assert!(USE_INVALID_BIT, "must use invalid bit");

    const FRAME_SIZE: u32 = 4;
    const FRAME_MASK: u64 = (1 << FRAME_SIZE) - 1;

    let mut stack = 0u64;
    let mut nodes = [node; (u64::BITS / FRAME_SIZE) as usize]; // TODO parent ptr.
    let mut sp = 0;

    loop {
        let dir = stack & FRAME_MASK;
        let node = *nodes.get_unchecked(sp);

        if dir > 8 {
            debug_assert!(false);
            std::hint::unreachable_unchecked();
        }

        if dir != 8 {
            stack += 1;

            let next_ptr = (ptr as isize + get_offset(dir as usize)) as *mut u8;
            if let Some(n) = CNode::get_child2(node.children(), *next_ptr, node.load_data_u64()) {
                if !n.mark(n.load_data()) {
                    // Push empty dir.
                    sp += 1;
                    stack <<= FRAME_SIZE;

                    // Push node.
                    debug_assert!(sp < nodes.len());
                    std::intrinsics::prefetch_read_data(n.children(), 3);
                    *nodes.get_unchecked_mut(sp) = n;

                    // Push ptr.
                    ptr = next_ptr;

                    // Set invalid.
                    *ptr |= INVALID_BIT;
                    continue;
                }
            }
        } else {
            if sp == 0 {
                return;
            }

            // Clear invalid.
            *ptr &= !INVALID_BIT;

            // Pop ptr.
            let next_ptr = ptr;
            let prev_dir = ((stack >> FRAME_SIZE) & FRAME_MASK) as u8 - 1;
            if prev_dir > 7 {
                debug_assert!(false);
                std::hint::unreachable_unchecked();
            }
            ptr = (ptr as isize - get_offset(prev_dir as usize)) as *mut u8;

            // Pop node.
            let prev_node = *nodes.get_unchecked(sp - 1);
            debug_assert_eq!(
                prev_node.index_child(*next_ptr, prev_node.load_data()) as *const CNode,
                node as *const CNode
            );

            // Pop full dir.
            debug_assert_eq!(stack & FRAME_MASK, 8);
            sp -= 1;
            stack >>= FRAME_SIZE;
        }
    }
}

unsafe fn c_find(node: &CNode, ptr: *mut u8) {
    let data = node.load_data_u64();
    if node.mark(data as u32) {
        return;
    }

    let children = node.children();

    // Prefetch read with locality 3 is about 10% faster (write is slower).
    std::intrinsics::prefetch_read_data(children, 3);
    let old = if USE_INVALID_BIT {
        *ptr |= INVALID_BIT;
        0
    } else {
        mem::replace(&mut *ptr, INVALID)
    };

    unroll! {
        for a in 0..3 {
            for b in 0..3 {
                let i = a as isize - 1;
                let j = b as isize - 1;

                if i == 0 && j == 0 {
                    continue;
                }

                let new_ptr = (ptr as isize + i * PADDED as isize + j) as *mut u8;
                let idx = *new_ptr;

                if let Some(sub) = CNode::get_child2(children, idx, data) {
                    c_find(sub, new_ptr)
                }
            }
        }
    }

    if USE_INVALID_BIT {
        *ptr &= !INVALID_BIT;
    } else {
        *ptr = old;
    }
}

unsafe fn c_find_simd(node: &CNode, ptr: *mut u8) {
    let data = node.load_data();
    // Slightly faster to not raise mark to caller.
    if node.mark(data) {
        return;
    }

    std::intrinsics::prefetch_read_data(node.children(), 3);

    let mut indices_index = 0usize;
    let mut set = 0u8;

    unroll! {
        for a in 0..3 {
            for b in 0..3 {
                let i = a as isize - 1;
                let j = b as isize - 1;

                if !(i == 0 && j == 0) {
                    let new_ptr = (ptr as isize + i * PADDED as isize + j) as *mut u8;
                    let idx = *new_ptr;
                    let has_child = node.has_child(idx, data);
                    set |= (has_child as u8) << indices_index;

                    indices_index += 1;
                }
            }
        }
    }

    // Don't need to edit board if not reading it.
    if set == 0 {
        return;
    }

    let old = mem::replace(&mut *ptr, INVALID);
    loop {
        // No more bits.
        if set == 0 {
            break;
        }

        // Find next bit.
        let indices_index = set.trailing_zeros();

        // Clear bit.
        set = set.blsr();

        // Get next board ptr from current and offset.
        let new_ptr = (ptr as isize + get_offset(indices_index as usize)) as *mut u8;
        let idx = *new_ptr;

        let child = node.index_child(idx, data);
        c_find_simd(child, new_ptr);
    }
    *ptr = old;
}

// Lookup indices_index to board ptr offset.
const fn get_offset(index: usize) -> isize {
    match SIZE {
        0..=124 => {
            const OFFSETS: [i8; 8] = offsets_8();
            OFFSETS[index] as isize
        }
        _ => {
            const OFFSETS: [i32; 8] = offsets_32();
            OFFSETS[index] as isize
        }
    }
}

macro_rules! impl_offsets {
    ($typ:ty) => {{
        let mut offsets_index = 0usize;
        let mut offsets = [0; 8];

        // Can't use for range in const fn.
        let mut a = 0;
        while a < 3 {
            let mut b = 0;
            while b < 3 {
                let i = a as isize - 1;
                let j = b as isize - 1;

                if !(i == 0 && j == 0) {
                    offsets[offsets_index] = (i * PADDED as isize + j) as $typ;
                    offsets_index += 1;
                }
                b += 1;
            }
            a += 1;
        }

        offsets
    }};
}

const fn offsets_8() -> [i8; 8] {
    // i8 offsets don't work above this.
    if SIZE >= (i8::MAX - 2) as usize {
        return [0; 8];
    }
    impl_offsets!(i8)
}

const fn offsets_32() -> [i32; 8] {
    // i32 offsets don't work above this.
    if SIZE >= (i32::MAX - 2) as usize {
        return [0; 8];
    }
    impl_offsets!(i32)
}

/// Prints the largest found words.
/// Also prints how many words the search found and how long it took.
fn print_found(mut words: Vec<Word>, word_count: usize, duration: Duration) {
    let found_count = words.len();

    // Partition up to MAX_OUTPUT words ~30x faster than sorting for 50x50 board.
    let start = words.len().saturating_sub(MAX_OUTPUT);
    let words = if start != 0 {
        words.select_nth_unstable(start - 1).2
    } else {
        &mut words
    };
    words.sort_unstable();

    let mut word_len: usize = 0;
    for w in words.iter() {
        let s = w.as_str();
        if s.len() != word_len {
            word_len = s.len();
            println!("\n{} Letter Words:", word_len)
        }
        println!("{}", s)
    }

    println!(
        "\nFound {}/{} words in {:?}!",
        found_count, word_count, duration
    )
}
