use rand::prelude::*;
use std::fmt;

/// Width/height of the board.
pub const SIZE: usize = 50;

/// Padded width/height of the board.
/// The board is padded with INVALID on the edges because it's faster than bounds checks.
pub const PADDED: usize = SIZE + 2;

fn is_padding(index: usize) -> bool {
    let x = index / PADDED;
    let y = index % PADDED;
    x == 0 || x == PADDED - 1 || y == 0 || y == PADDED - 1
}

/// Convert between alphabet and possibly zero index.
const SORTED_ALPHABET: bool = true;
const INDEX_TO_LETTER: [u8; 26] = *b"eiasrntolcudpmghbyfvkwzxqj"; // Sorted by most common letters.
const LETTER_TO_INDEX: [u8; 26] = [
    2, 16, 9, 11, 0, 18, 14, 15, 1, 25, 20, 8, 13, 5, 7, 12, 24, 4, 3, 6, 10, 19, 21, 23, 17, 22,
];

/// Used to signify end of board or already searched (not a real index/letter).
pub const INVALID: u8 = 0;

// Used to signify that a word is invalid.
pub const INVALID_BIT: u8 = 1 << 5;

pub fn index_to_capital_letter(i: usize) -> u8 {
    let i = i - 1; // Remove space for INVALID.
    if SORTED_ALPHABET {
        INDEX_TO_LETTER[i] - (b'a' - b'A')
    } else {
        i as u8 + b'A'
    }
}

pub fn letter_to_index(a: u8) -> u8 {
    let i = a - b'a';
    (if SORTED_ALPHABET {
        LETTER_TO_INDEX[i as usize]
    } else {
        i
    }) + 1 // Make space for INVALID.
}

pub struct Board {
    pub board: [u8; PADDED * PADDED],
}

impl Board {
    pub fn new() -> Box<Self> {
        let mut board = box Self {
            board: [0u8; PADDED * PADDED],
        };
        board.generate();
        board
    }

    fn generate(&mut self) {
        #[cfg(not(feature = "seeded"))]
        let mut rng = thread_rng();

        #[cfg(feature = "seeded")]
        let mut rng = StdRng::seed_from_u64(42);

        for (index, i) in self.board.iter_mut().enumerate() {
            // Pad edges of the board.
            *i = if is_padding(index) {
                INVALID
            } else {
                letter_to_index(gen_distribution(&mut rng) + b'a')
            };
        }
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if SIZE > 50 {
            return Ok(());
        }

        let mut last_padding = false;
        for (index, i) in self.board.iter().enumerate() {
            last_padding = if is_padding(index) {
                if !last_padding {
                    writeln!(f)?;
                }
                true
            } else {
                write!(f, "{} ", index_to_capital_letter(*i as usize) as char)?;
                false
            };
        }
        write!(f, "")
    }
}

fn gen_distribution<R: Rng>(rng: &mut R) -> u8 {
    let r: u32 = rng.gen();
    find(r)
}

#[inline(never)]
fn find(r: u32) -> u8 {
    let values = [
        347006895, 427847953, 612368025, 759390464, 1244617579, 1299387272, 1412305847, 1516509671,
        1897040225, 1904244692, 1937065689, 2171944139, 2298423206, 2592820874, 2877008083,
        3008729413, 3016729841, 3330711647, 3668607267, 3960850301, 4111071163, 4153324151,
        4185655326, 4198857198, 4277132521,
    ];
    values
        .iter()
        .copied()
        .map(|e| if e < r { 1 } else { 0 })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::intrinsics::black_box;
    use test::Bencher;

    const DISTRIBUTION: [f64; 25] = [
        0.08079383882234344,
        0.09961611437144244,
        0.14257804147050937,
        0.1768093707466253,
        0.28978511229757753,
        0.3025371751312712,
        0.3288280796554193,
        0.35308992291739494,
        0.4416890967656532,
        0.4433665174343813,
        0.45100825133477473,
        0.5056951520438261,
        0.5351433546376846,
        0.6036881531740994,
        0.6698556440691198,
        0.7005244060973664,
        0.7023871508626396,
        0.7754917367854756,
        0.8541641917187962,
        0.9222073253931349,
        0.9571833452696533,
        0.9670211357719631,
        0.9745488240744826,
        0.977622624474321,
        0.9958475183202712,
    ];

    #[bench]
    fn bench_gen_distribution(b: &mut Bencher) {
        let mut rng = StdRng::seed_from_u64(42);
        let distribution = &DISTRIBUTION.map(|v| (v * u32::MAX as f64) as u32);
        println!("{distribution:?}");
        b.iter(|| {
            black_box(gen_distribution(&mut rng));
        })
    }
}
