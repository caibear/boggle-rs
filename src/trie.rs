use crate::board::{index_to_capital_letter, letter_to_index};
use crate::USE_INVALID_BIT;
use bitintr::Bzhi;
use std::alloc;
use std::io::Write;
use std::num::NonZeroU16;
use std::num::NonZeroU32;
use std::str;
use std::sync::atomic::*;

pub const MAX_WORD_LENGTH: usize = 28;
pub const CAPACITY: u32 = 426853;

const CHARS: usize = 26;
const INVALID_AND_CHARS: usize = CHARS + 1; // Chars and space for invalid.

// Data bits (per node).
// 32 bit is ~1% faster even though only 8 bits are needed.
type AtomicData = AtomicU32;
type Data = u32;
const REF_BITS: Data = 0b00011111; // Count of non-deleted children is 5 bits because log2(26) = 5.
const NOT_WORD_BIT: Data = 0b00100000; // Is this node not a word (!word saves work in mark).
const FOUND_BIT: Data = 0b01000000; // Has this node been found.
const _UNUSED_BIT: Data = 0b10000000; // Previously used as DELETED_BIT.
const DEFAULT_BITS: Data = NOT_WORD_BIT;

pub struct Trie {
    nodes: Box<[Node; CAPACITY as usize]>,
    alloc_index: u32,

    data: AtomicData,
    children: [AtomicU32; INVALID_AND_CHARS], // 0 means None.
}

// TODO Fix maintaining duplicated 32bit versions of Node methods possibly with macros.
impl Trie {
    pub fn new() -> Self {
        let nodes = unsafe {
            let layout = alloc::Layout::new::<[Node; CAPACITY as usize]>();
            let ptr = alloc::alloc_zeroed(layout) as *mut [Node; CAPACITY as usize];
            Box::from_raw(ptr)
        };

        #[allow(clippy::declare_interior_mutable_const)]
        const ATOMIC_32_ZERO: AtomicU32 = AtomicU32::new(0);

        Self {
            nodes,
            alloc_index: 0,

            // Root node.
            data: AtomicData::new(DEFAULT_BITS),
            children: [ATOMIC_32_ZERO; INVALID_AND_CHARS],
        }
    }

    fn alloc(alloc_index: &mut u32) -> u32 {
        *alloc_index += 1;
        debug_assert!(*alloc_index < CAPACITY);
        *alloc_index
    }

    /// offset offsets from the root node.
    pub unsafe fn offset(&self, offset: u32) -> &Node {
        self.nodes.get_unchecked(offset as usize)
    }

    pub unsafe fn index_child(&self, i: u8) -> Option<NonZeroU32> {
        debug_assert!(self.children.len() > i as usize);
        NonZeroU32::new(
            self.children
                .get_unchecked(i as usize)
                .load(Ordering::Relaxed),
        )
    }

    pub unsafe fn delete_child(&self, i: u8) -> bool {
        // Delete child.
        debug_assert!(self.children.len() > i as usize);
        self.children
            .get_unchecked(i as usize)
            .store(0, Ordering::Relaxed);

        // Decrement ref count and return if the child's deletion caused this node to have 0 refs.
        // This deletion isn't racy because fetch_sub will only make ref bits 0 for 1 thread.
        // Has 0 refs if the previous was 1 ref.
        self.data.fetch_sub(1, Ordering::Relaxed) & REF_BITS == 1
    }

    pub fn add(&mut self, w: &[u8]) -> usize {
        assert!(w.len() <= MAX_WORD_LENGTH);

        let mut count = 0;
        if !w.is_empty() {
            let i = letter_to_index(w[0]);

            let offset = match unsafe { self.index_child(i) } {
                Some(index) => index,
                None => {
                    let a = Trie::alloc(&mut self.alloc_index);
                    *self.nodes[a as usize].data.get_mut() = DEFAULT_BITS;
                    let index = unsafe { NonZeroU32::new_unchecked(a) };

                    self.children[i as usize] = index.get().into();
                    count += 1;

                    debug_assert!(*self.data.get_mut() & REF_BITS < CHARS as Data);
                    *self.data.get_mut() += 1;

                    index
                }
            }
            .get();
            count += unsafe { Node::add(offset, self, &w[1..]) };
        } else {
            *self.data.get_mut() &= !NOT_WORD_BIT;
        }
        count
    }

    fn iter_nonzero_children(&self) -> impl Iterator<Item = (usize, u32)> + '_ {
        let child_count = (self.data.load(Ordering::Relaxed) & REF_BITS) as usize;
        self.children
            .iter()
            .enumerate()
            .skip(1)
            .filter_map(|(index, i)| {
                let idx = i.load(Ordering::Relaxed);
                (idx != 0).then_some((index, idx))
            })
            .take(child_count)
    }

    pub fn collect(&self, original: &Self) -> Vec<Word> {
        let mut vec = vec![];
        let mut buf = [0; MAX_WORD_LENGTH];

        for (index, idx) in original.iter_nonzero_children() {
            buf[0] = index_to_capital_letter(index);
            unsafe {
                let sub = self.offset(idx);
                let sub_original = original.offset(idx);
                Node::collect(sub, sub_original, &mut vec, &mut buf, 1);
            }
        }

        vec
    }
}

/// Node is a node of the trie.
#[derive(Debug)]
#[repr(align(64))]
pub struct Node {
    data: AtomicData,
    pub children: [AtomicU16; INVALID_AND_CHARS], // 0 means None.
}

impl Node {
    unsafe fn add(self_index: u32, trie: &mut Trie, w: &[u8]) -> usize {
        // Use unsafe to avoid double borrow when setting not word bit.
        let self_node = &mut *trie.nodes.as_mut_ptr().add(self_index as usize);

        let mut count = 0;
        if !w.is_empty() {
            let i = letter_to_index(w[0]);

            let offset = match self_node.index_child(i) {
                Some(index) => index,
                None => {
                    let a = Trie::alloc(&mut trie.alloc_index);
                    debug_assert!(a > self_index);
                    *trie.nodes[a as usize].data.get_mut() = DEFAULT_BITS;
                    let index = NonZeroU16::new_unchecked((a - self_index) as u16);

                    self_node.children[i as usize] = index.get().into();
                    count += 1;

                    debug_assert!(*self_node.data.get_mut() & REF_BITS < CHARS as Data);
                    *self_node.data.get_mut() += 1;

                    index
                }
            }
            .get();
            count += Self::add(self_index + offset as u32, trie, &w[1..]);
        } else {
            *self_node.data.get_mut() &= !NOT_WORD_BIT;
        }
        count
    }

    fn iter_nonzero_children(&self) -> impl Iterator<Item = (usize, u16)> + '_ {
        let child_count = (self.data.load(Ordering::Relaxed) & REF_BITS) as usize;
        self.children
            .iter()
            .enumerate()
            .skip(1)
            .filter_map(|(index, i)| {
                let idx = i.load(Ordering::Relaxed);
                (idx != 0).then_some((index, idx))
            })
            .take(child_count) // Makes the most difference with a sorted alphabet.
    }

    unsafe fn collect(
        &self,
        original: &Self,
        vec: &mut Vec<Word>,
        buf: &mut [u8; MAX_WORD_LENGTH],
        depth: usize,
    ) {
        let data = self.data.load(Ordering::Relaxed);
        if data & FOUND_BIT != 0 {
            vec.push(Word {
                buf: *buf,
                len: depth as u8,
            })
        } else if data & NOT_WORD_BIT == 0 {
            // Open words mean this node was never reached.
            return;
        }

        for (i, index) in original.iter_nonzero_children() {
            *buf.get_unchecked_mut(depth) = index_to_capital_letter(i);
            let sub = self.offset(index);
            let sub_original = original.offset(index);
            sub.collect(sub_original, vec, buf, depth + 1);
        }
    }

    pub fn mark(&self) -> bool {
        let data = self.data.load(Ordering::Relaxed);
        // Is a word that hasn't been found.
        if data & (NOT_WORD_BIT | FOUND_BIT) == 0 {
            // Mark that the word was found and the word.
            // Only return deleted if it was a non found word with no children.
            self.data.fetch_or(FOUND_BIT, Ordering::Relaxed) == 0

            // Could return 2 bool 1 return and 2 return value.
            // If node was already deleted (race) could still early return, but with false.
            // Probably not worth it because the races are few and far between.
        } else {
            false
        }
    }

    pub unsafe fn index_child(&self, i: u8) -> Option<NonZeroU16> {
        debug_assert!(self.children.len() > i as usize);
        NonZeroU16::new(
            self.children
                .get_unchecked(i as usize)
                .load(Ordering::Relaxed),
        )
    }

    pub unsafe fn delete_child(&self, i: u8) -> bool {
        // Delete child.
        debug_assert!(self.children.len() > i as usize);
        self.children
            .get_unchecked(i as usize)
            .store(0, Ordering::Relaxed);

        // Decrement ref count and return if the child's deletion caused this node to have 0 refs.
        // This deletion isn't racy because fetch_sub will only make ref bits 0 for 1 thread.
        // Has 0 refs if the previous was 1 ref.
        self.data.fetch_sub(1, Ordering::Relaxed) & REF_BITS == 1
    }

    pub unsafe fn offset(&self, offset: u16) -> &Node {
        &*(self as *const Node).add(offset as usize)
    }
}

#[repr(align(32))]
pub struct Word {
    buf: [u8; MAX_WORD_LENGTH],
    len: u8,
}

impl Word {
    pub fn as_str(&self) -> &str {
        unsafe { str::from_utf8_unchecked(&self.buf[0..self.len as usize]) }
    }
}

impl Eq for Word {}

impl PartialEq<Self> for Word {
    fn eq(&self, other: &Self) -> bool {
        self.as_str() == other.as_str()
    }
}

impl PartialOrd<Self> for Word {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Word {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let a = self.as_str();
        let b = other.as_str();
        a.len().cmp(&b.len()).then_with(|| a.cmp(b))
    }
}

pub struct CTrie {
    nodes: Box<[CNode; Self::CAPACITY]>,
}

impl CTrie {
    const CAPACITY: usize = CAPACITY as usize;

    fn empty() -> Self {
        #[allow(clippy::declare_interior_mutable_const)]
        const C_NODE_ZERO: CNode = CNode {
            data: AtomicU32::new(0),
            children: 0,
        };
        Self {
            nodes: box [C_NODE_ZERO; Self::CAPACITY as usize],
        }
    }

    #[allow(unused)]
    pub fn from_trie(trie: &Trie) -> Self {
        let mut c_trie = Self::empty();

        // Start alloc at 1 because root is already allocated.
        let mut alloc = 1;
        unsafe {
            c_trie.nodes[0].add_root(trie, &mut alloc);
        }

        c_trie
    }

    #[allow(unused)]
    pub fn export(&self) -> &[u8] {
        unsafe { self.nodes.align_to().1 }
    }

    pub fn import(bytes: &[u8]) -> Self {
        // Make sure it's the right number of bytes.
        assert_eq!(bytes.len(), std::mem::size_of::<CNode>() * Self::CAPACITY);

        let mut c_trie = Self::empty();

        // Copy the memory of bytes into c_trie.
        let mut nodes: &mut [u8] = unsafe { c_trie.nodes.align_to_mut().1 };
        nodes.write_all(bytes).unwrap();

        c_trie
    }

    pub fn checksum(&self) -> u32 {
        let mut sum = 0u32;
        for node in self.nodes.iter() {
            sum = sum
                .wrapping_add(node.load_data())
                .wrapping_add(node.children_offset());
        }
        sum
    }

    pub fn collect(&self) -> Vec<Word> {
        let mut vec = vec![];
        let mut buf = [0; MAX_WORD_LENGTH];
        unsafe {
            self.root().collect(&mut vec, &mut buf, 0);
        }
        vec
    }

    pub fn root(&self) -> &CNode {
        &self.nodes[0]
    }
}

#[repr(align(8))]
pub struct CNode {
    data: AtomicU32, // invalid bit + 26 children bits (a-z) + not word bit + found bit
    children: u32,   // pointer offset to dynamic array of children
}

impl CNode {
    const NOT_WORD_BIT: u32 = 1 << 31; // Is this node a not word (!word saves work in mark).
    const FOUND_BIT: u32 = 1 << 30; // Has this node been found.
    const REF_BITS: u32 = !(Self::NOT_WORD_BIT | Self::FOUND_BIT); // Bits that represent children.

    pub fn load_data(&self) -> u32 {
        self.data.load(Ordering::Relaxed)
    }

    pub fn load_data_u64(&self) -> u64 {
        if USE_INVALID_BIT && cfg!(target_arch = "x86_64") {
            let ptr: *const u32 = self.data.as_mut_ptr();
            let mut data: u64;

            // Convince the compiler that the upper 32 bits may not be zero without slow black box.
            // If we don't do this the compiler won't use 64 bit BT instructions and will use a
            // slower combination of SHLX and TEST.
            unsafe {
                use std::arch::asm;
                asm!(
                    "mov {data:e}, [{ptr}]",
                    ptr = in(reg) ptr,
                    data = out(reg) data,
                    options(pure, nomem, nostack, preserves_flags),
                )
            }
            data
        } else {
            self.load_data() as u64
        }
    }

    /// Marks a node as found (returns true iif node doesn't have children).
    pub fn mark(&self, data: u32) -> bool {
        // If this it's a word that hasn't been found mark it as found.
        // Don't mark otherwise to save atomic writes.
        if data & (Self::NOT_WORD_BIT | Self::FOUND_BIT) == 0 {
            // Doesn't need to be a fetch_or because it's the only bit being changed concurrently.
            // TODO look into if loading again is faster (it saves 2 instructions).
            self.data.store(
                self.data.load(Ordering::Relaxed) | Self::FOUND_BIT,
                Ordering::Relaxed,
            );
        }
        // Early return if there aren't any children (ignoring not word and found).
        data & Self::REF_BITS == 0
    }

    /// Returns a child at index `i` if it exists or None.
    /// Requires `data` to be passed for fewer loads.
    #[allow(unused)]
    pub unsafe fn get_child(&self, i: u8, data: u32) -> Option<&Self> {
        if !self.has_child(i, data) {
            None
        } else {
            Some(self.index_child(i, data))
        }
    }

    /// Like [`Self::get_child`] but with 64 bit data to use [`crate::board::INVALID_BIT`].
    /// Also requires children instead of self.
    pub unsafe fn get_child2(children: &Self, i: u8, data: u64) -> Option<&Self> {
        let has_child = if USE_INVALID_BIT {
            data & (1 << i) == 0
        } else {
            data as u32 & (1 << i) == 0
        };
        if has_child {
            None
        } else {
            Some(Self::index_child_inner(children, i, data as u32))
        }
    }

    /// First half of get_child (separated for simd version).
    pub unsafe fn has_child(&self, i: u8, data: u32) -> bool {
        if i >= 27 {
            debug_assert!(false);
            std::hint::unreachable_unchecked();
        }

        data & (1 << i) != 0
    }

    /// Second half of get_child (separated for simd version).
    pub unsafe fn index_child(&self, i: u8, data: u32) -> &Self {
        Self::index_child_inner(self.children(), i, data)
    }

    unsafe fn index_child_inner(children: &Self, i: u8, data: u32) -> &Self {
        if i == 0 || i >= 27 {
            debug_assert!(false);
            std::hint::unreachable_unchecked();
        }

        // Offset by number of present children before the child.
        // let bits = data << (32 - i) as u32;
        // Using bzhi is 1 less instruction over ^^.
        let bits = data.bzhi(i as u32);
        let index = bits.count_ones();
        &*(children as *const Self).add(index as usize)
    }

    /// Ref to first child or self (used by prefetch and faster offsets).
    pub fn children(&self) -> &Self {
        unsafe { &*(self as *const Self).byte_add(self.children as usize) }
    }

    /// Mutable ref to first child or self (used by alloc).
    fn children_mut(&mut self) -> &mut Self {
        unsafe { &mut *(self as *mut Self).byte_add(self.children as usize) }
    }

    fn children_offset(&self) -> u32 {
        self.children / std::mem::size_of::<Self>() as u32
    }

    /// **Safety**
    ///
    /// Must provide a valid offset within the CNode array.
    unsafe fn set_children_offset(&mut self, offset: u32) {
        self.children = offset * std::mem::size_of::<Self>() as u32;
    }

    unsafe fn alloc_child_mut(&mut self, i: u8) -> (&mut Self, u32) {
        debug_assert!(27 > i as usize);
        let data = self.data.get_mut();
        let bit = 1 << i;
        *data |= bit;

        let mask = bit - 1;
        let index = (*data & mask).count_ones();
        let child = &mut *(self.children_mut() as *mut Self).add(index as usize);
        (child, index)
    }

    // Adds a Trie to itself.
    // Cannot add words directly because need to know each node's child count ahead of time.
    unsafe fn add_root(&mut self, root: &Trie, alloc: &mut u32) {
        let data = root.data.load(Ordering::Relaxed);
        // Inverted because not word vs word.
        if data & NOT_WORD_BIT != 0 {
            *self.data.get_mut() |= Self::NOT_WORD_BIT;
        }

        // Allocate enough space for exactly enough children.
        let next = *alloc;
        self.set_children_offset(next);
        *alloc += (data & REF_BITS) as u32;
        debug_assert!(*alloc <= CTrie::CAPACITY as u32);

        for (index, idx) in root.iter_nonzero_children() {
            let (child, offset) = self.alloc_child_mut(index as u8);
            child.add(root.offset(idx), next + offset, alloc);
        }
    }

    unsafe fn add(&mut self, node: &Node, self_index: u32, alloc: &mut u32) {
        let data = node.data.load(Ordering::Relaxed);
        // Inverted because not word vs word.
        if data & NOT_WORD_BIT != 0 {
            *self.data.get_mut() |= Self::NOT_WORD_BIT;
        }

        // Allocate enough space for exactly enough children.
        let next = *alloc;
        self.set_children_offset(next - self_index);
        *alloc += (data & REF_BITS) as u32;
        debug_assert!(*alloc <= CTrie::CAPACITY as u32);

        for (index, idx) in node.iter_nonzero_children() {
            let (child, offset) = self.alloc_child_mut(index as u8);
            child.add(node.offset(idx), next + offset, alloc);
        }
    }

    unsafe fn collect(&self, vec: &mut Vec<Word>, buf: &mut [u8; MAX_WORD_LENGTH], depth: usize) {
        let data = self.load_data();
        if data & Self::FOUND_BIT != 0 {
            vec.push(Word {
                buf: *buf,
                len: depth as u8,
            });

            // Early return if there aren't any children.
            if data & Self::REF_BITS == 0 {
                return;
            }
        } else if data & Self::NOT_WORD_BIT == 0 {
            // Early return because it's a word that wasn't found so it's children weren't found.
            return;
        }

        // Use CTZ instead of looping through all the children (> 2x faster than iter).
        let data = self.load_data();
        let mut bits = data & Self::REF_BITS;

        loop {
            // Find next bit (can ignore invalid bit because it's never set).
            let i = bits.trailing_zeros();
            if i == u32::BITS {
                break;
            }

            if i < 1 || i > 26 {
                debug_assert!(false);
                std::hint::unreachable_unchecked();
            }

            // Clear bit.
            bits &= !(1 << i);

            // Index children with bit's index.
            let child = self.index_child(i as u8, data);
            *buf.get_unchecked_mut(depth) = index_to_capital_letter(i as usize);
            child.collect(vec, buf, depth + 1);
        }
    }
}
